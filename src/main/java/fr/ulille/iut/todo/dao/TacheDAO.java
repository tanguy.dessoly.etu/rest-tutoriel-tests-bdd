package fr.ulille.iut.todo.dao;

import java.util.List;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import fr.ulille.iut.todo.service.Tache;

/**
 * TacheDAO
 */
public interface TacheDAO {
    @SqlUpdate("create table if not exists taches (id varchar(128) primary key, nom varchar not null, description varchar)")
    void createTable();

    @SqlUpdate("drop table if exists taches")
    void dropTable();

    @SqlUpdate("insert into taches (id, nom, description) values (:id, :nom, :description)")
    int insert(@BindBean Tache tache);
    
    @SqlUpdate("update taches set nom = :nom, description = :description WHERE id = :id_origin")
    int update(@Bind("id_origin") String id_origine, @BindBean Tache tache);
    
    @SqlUpdate("delete from taches where id=?")
    @RegisterBeanMapper(Tache.class)
    int delete(String id);

    @SqlQuery("select * from taches")
    @RegisterBeanMapper(Tache.class)
    List<Tache> getAll();
    
    @SqlQuery("select * from taches where id=?")
    @RegisterBeanMapper(Tache.class)
	Tache getById(String id);
}

