package fr.ulille.iut.todo.service;

import java.util.UUID;


import fr.ulille.iut.todo.dto.CreationTacheDTO;
import jakarta.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Tache {
    private UUID id = UUID.randomUUID();
    private String nom;
    private String description;

    public Tache() {}

    public static Tache fromCreationTacheDTO(CreationTacheDTO dto) {
        Tache tache = new Tache();
        tache.setNom(dto.getNom());
        tache.setDescription(dto.getDescription());

        return tache;
    }

	public UUID getId() {
		return id;
	}

    public void setId(UUID id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	@Override
	public String toString() {
		return "Tache [description=" + description + ", id=" + id + ", nom=" + nom + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tache other = (Tache) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		return true;
	}

}
