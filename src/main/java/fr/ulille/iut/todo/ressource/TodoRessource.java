package fr.ulille.iut.todo.ressource;

import java.net.URI;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

import fr.ulille.iut.todo.dto.CreationTacheDTO;
import fr.ulille.iut.todo.service.Tache;
import fr.ulille.iut.todo.service.TodoService;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.EntityTag;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.NoContentException;
import jakarta.ws.rs.core.Request;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.ResponseBuilder;
import jakarta.ws.rs.core.UriInfo;

@Path("taches")
@Produces(MediaType.APPLICATION_JSON)
public class TodoRessource {
    private final static Logger LOGGER = Logger.getLogger(TodoRessource.class.getName());

    private TodoService todoService = new TodoService();

    @Context
    private UriInfo uri;
    @Context
    Request request;

    @GET
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public List<Tache> getAll() {
        LOGGER.info("getAll()");

        return todoService.getAll();
    }
    
    @GET
    @Path("{id}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Tache getById(@PathParam("id") String id) {
        LOGGER.info("getById(String id)");

        Tache tache = todoService.getTache(UUID.fromString(id));
        if(tache == null) {
        	throw new NotFoundException();
        }
        return tache;
    }
    
    @GET
    @Path("{id}/description")
    @Produces({ MediaType.TEXT_PLAIN })
    public String getDescriptionById(@PathParam("id") String id) {
        LOGGER.info("getDesciptionById(String id)");

        Tache tache = todoService.getTache(UUID.fromString(id));
        if(tache == null) {
        	throw new NotFoundException();
        }
        return tache.getDescription();
    }

    @POST
    public Response createTache(CreationTacheDTO tacheDto) {
        LOGGER.info("createTache()");

        Tache tache = Tache.fromCreationTacheDTO(tacheDto);
        todoService.addTache(tache);
        URI location = uri.getAbsolutePathBuilder().path(tache.getId().toString()).build();

        EntityTag etag = new EntityTag(Integer.toString(tache.hashCode()));
        ResponseBuilder builder = request.evaluatePreconditions(etag);

        if (builder == null) {
            builder = Response.created(location);
            builder.tag(etag);
            builder.entity(tache);
        }
        return builder.build();
    }
    
    @DELETE
    @Path("{id}")
    public void deleteTache(@PathParam("id") String id) {
        LOGGER.info("deleteTache(String id)");
        int res = todoService.deleteTache(UUID.fromString(id));
        if(res < 1) throw new NotFoundException();
        
    }
    
    @PUT
    @Path("{id}")
    public int putTache(@PathParam("id") String id, CreationTacheDTO tache) {
        LOGGER.info("putTache(String id)");
        return todoService.updateTache(id, Tache.fromCreationTacheDTO(tache));
    }
}
